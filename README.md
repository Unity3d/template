# README #

This is Unity3d Template configured for Git includes

* Gitignore for ignoring unnecessary Unity3d and Visual Studio files and folders
* Gitattributes for Git Large File Storage of assets tracking and serialization types
* Unity3d project settings (currently Unity 2017.1.0f3)
* Project asset structure as a standard